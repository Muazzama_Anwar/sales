from pymongo import MongoClient
import datetime
from dateutil import parser

class database():
    server = 'ds251197.mlab.com'
    port = 51197
    db_name = 'sales_management'
    username = 'muazzama'
    password = '01dec1995'
    conn = MongoClient('mongodb://muazzama:01dec1995@ds251197.mlab.com:51197/sales_management')
    db = conn['sales_management']
    db.authenticate(username, password)
    client = db['client']
    distributor = db['distributor']
    sales = db['sales']
    purchase = db['purchase']
    product = db['product']

    def insert_client(self, name, address, contact, client_id, created_at, discount):
        try:
            self.client.insert_one({"name": name, "address": address, "contact": contact, "client_id":client_id, "created_at":created_at, "discount":discount})
            print("client inserted")
        except Exception as e:
            print(e)


    def insert_distributor(self, name, address, contact, distributor_id, created_at):
        try:
            self.distributor.insert_one({"name": name, "address": address, "contact": contact, "distributor_id":int(distributor_id), "created_at":created_at})
            print("distributor inserted")
        except Exception as e:
            print(e)


    def insert_sales(self, client, client_id, product, quantity, total, paid, remaining, record_id, created_at):
        try:
            self.sales.insert_one({"client":client, "client_id":client_id, "product":product, "quantity":quantity,
                                    "total":total, "paid":paid, "remaining":remaining,
                                    "record_id":record_id, "created_at":created_at})
            print("distributor inserted")
        except Exception as e:
            print(e)

    def insert_purchase(self, distributor, distributor_id, product, quantity, total, paid, remaining, record_id, created_at):
        try:
            self.purchase.insert_one({"distributor":distributor, "distributor_id":distributor_id,
                                    "product":product, "quantity":quantity, "total":total,
                                    "paid":paid, "remaining":remaining, "record_id":record_id, "created_at":created_at})
            print("distributor inserted")
        except Exception as e:
            print(e)

    def insert_product(self, product, net_price, distributor, product_id, created_at, sales_price):
        try:
            self.product.insert_one({"product":product, "net_price":net_price,
                                    "distributor":distributor, "product_id":product_id,
                                    "created_at":created_at, "sales_price": sales_price})
            print("product inserted")
        except Exception as e:
            print(e)


    def retrieve_client(self):
        try:
            cursor = self.client.find({})
            return cursor
        except Exception as e:
            print(e)

    def retrieve_distributor(self):
        try:
            cursor = self.distributor.find({})
            return cursor
        except Exception as e:
            print(e)

    def retrieve_sales(self):
        try:
            cursor = self.sales.find({})
            return cursor
        except Exception as e:
            print(e)

    def retrieve_purchase(self):
        try:
            cursor = self.purchase.find({})
            return cursor
        except Exception as e:
            print(e)

    def retrieve_product(self):
        try:
            cursor = self.product.find({})
            return cursor
        except Exception as e:
            print(e)

    def delete_client(self, record_id):
        try:
            self.client.remove({"client_id":int(record_id)})
            return "client record deleted."
        except Exception as e:
            print(e)

    def delete_distributor(self, record_id):
        try:
            self.distributor.remove({"distributor_id":int(record_id)})
            return "distributor record deleted."
        except Exception as e:
            print(e)

    def delete_sales(self, record_id):
        try:
            self.sales.remove({"record_id":int(record_id)})
            return "sales record deleted."
        except Exception as e:
            print(e)

    def delete_purchase(self, record_id):
        try:
            self.purchase.remove({"record_id":int(record_id)})
            return "purchase record deleted."
        except Exception as e:
            print(e)

    def delete_product(self, record_id):
        try:
            self.product.remove({"product_id":int(record_id)})
            return "product record deleted."
        except Exception as e:
            print(e)

    def distinct_client_id(self):
        distinct = self.client.distinct('client_id')
        return distinct

    def distinct_distributor_id(self):
        distinct = self.distributor.distinct('distributor_id')
        return distinct

    def distinct_sales_id(self):
        distinct = self.sales.distinct('record_id')
        return distinct

    def distinct_purchase_id(self):
        distinct = self.purchase.distinct('record_id')
        return distinct

    def distinct_product_id(self):
        distinct = self.product.distinct('product_id')
        return distinct

    def get_dues(self, id):
        records = self.sales.find({"client_id":id})
        dues = 0
        for r in records:
            dues += r['remaining']
        return dues

    def get_report(self):
        
        return