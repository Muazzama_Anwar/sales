from flask import Flask, url_for, render_template, redirect, request
from app.models import database
from datetime import datetime
async_mode = None
app = Flask(__name__)

db = database()
now = datetime.now().date()
@app.route('/')
def index():
    return render_template("login.html")

@app.route('/login',methods=['GET', 'POST'])
def login():
    email = request.form['email']
    password = request.form['password']
    print(email,password)
    if email=="anwarahmed012@gmail.com" and password=="anwar123":
        return redirect(url_for("sales"))
    else:
        return redirect(url_for("index"))

@app.route('/client', methods = ['GET','POST'])
def client():
    cursor = db.retrieve_client()
    client_list = []
    if cursor is not None:
        for c in cursor:
            dues = db.get_dues(str(c['client_id']))
            c['dues'] = dues
            client_list.append(c)
    return render_template("client.html", data = client_list)

@app.route('/distributor', methods = ['GET','POST'])
def distributor():
    cursor = db.retrieve_distributor()
    distributor_list = []
    for c in cursor:
        distributor_list.append(c)
    print(distributor_list)
    return render_template("distributor.html", data = distributor_list)

@app.route('/sales', methods = ['GET','POST'])
def sales():
    product_cursor = db.retrieve_product()
    product_list = []
    for product in product_cursor:
        product_list.append(product)
    client_cursor = db.retrieve_client()
    client_list = []
    for client in client_cursor:
        client_list.append(client)
    cursor = db.retrieve_sales()
    sales_list = []
    for c in cursor:
        sales_list.append(c)
    return render_template("sales.html", data = sales_list, client_data = client_list, product_data = product_list)

@app.route('/purchase', methods = ['GET','POST'])
def purchase():
    product_cursor = db.retrieve_product()
    product_list = []
    for product in product_cursor:
        product_list.append(product)
    distributor_cursor = db.retrieve_distributor()
    distributor_list = []
    for distributor in distributor_cursor:
        distributor_list.append(distributor)
    cursor = db.retrieve_purchase()
    purchase_list = []
    for purchase in cursor:
        purchase_list.append(purchase)
    return render_template("purchase.html", data = purchase_list, distributor_data = distributor_list, product_data = product_list)

@app.route('/products', methods = ['GET','POST'])
def products():
    product_cursor = db.retrieve_product()
    product_list = []
    for product in product_cursor:
        product_list.append(product)
    distributor_list = []
    distributor_cursor = db.retrieve_distributor()
    for distributor in distributor_cursor:
        distributor_list.append(distributor)
    return render_template("products.html", data = product_list, distributor_data = distributor_list)

@app.route('/report', methods = ['GET', 'POST'])
def report():

    return render_template('report.html')

@app.route('/add_client', methods = ['GET','POST'])
def add_client():
    client_id = 0
    name = request.form['name']
    address = request.form['address']
    contact = request.form['contact']
    discount = request.form['discount']
    record_id = db.distinct_client_id()
    if len(record_id) == 0:
        client_id = 1
    else:
        client_id = max(record_id)+1
    db.insert_client(name, address,contact, client_id ,str(now), discount)
    return redirect(url_for("client"))

@app.route('/add_distributor', methods = ['GET', 'POST'])
def add_distributor():
    distributor_id = 0
    name = request.form['name']
    address = request.form['address']
    contact = request.form['contact']
    record_id = db.distinct_distributor_id()
    if len(record_id)==0:
        distributor_id = 1
    else:
        distributor_id = max(record_id)+1
    db.insert_distributor(name, address, contact,distributor_id , str(now))
    return redirect(url_for("distributor"))

@app.route('/add_sales',methods = ['GET', 'POST'])
def add_sales():
    sales_id = 0
    name,price = (request.form['product']).split(',')
    print(name, price)
    id, discount, client = (request.form['client']).split(',')
    print(client, id, discount)
    quantity = int(request.form['quantity'])
    total = (quantity*float(price))-((quantity*float(price)*int(discount))/100) #request.form['total']
    paid = float(request.form['paid'])
    date = request.form['date']
    record_id = db.distinct_sales_id()
    if len(record_id) == 0:
        sales_id = 1
    else:
        sales_id = max(record_id) + 1
    db.insert_sales(client, id, name, quantity, total, paid, total-paid, sales_id, date)
    return redirect(url_for("sales"))

@app.route('/add_purchase',methods= ['GET', 'POST'])
def add_purchase():
    purchase_id = 0
    name, price = (request.form['product']).split(',')
    print(name, price)
    id, distributor = (request.form['client']).split(',')
    print(distributor, id)
    quantity = int(request.form['quantity'])
    total = (quantity * float(price)) # request.form['total']
    paid = float(request.form['paid'])
    date = request.form['date']
    record_id = db.distinct_purchase_id()
    if len(record_id) == 0:
        purchase_id = 1
    else:
        purchase_id = max(record_id) + 1
    db.insert_purchase(distributor, id, name, quantity, total, paid, total - paid, purchase_id, date)
    return redirect(url_for("purchase"))

@app.route('/add_product',methods = ['GET', 'POST'])
def add_product():
    product_id = 0
    name = request.form['product']
    distributor = request.form['distributor']
    price = request.form['price']
    sales_price = request.form['sales_price']
    record_id = db.distinct_product_id()
    if len(record_id) == 0:
        product_id = 1
    else:
        product_id = max(record_id) + 1
    db.insert_product(name, float(price), distributor, product_id, str(now), float(sales_price))
    return redirect(url_for("products"))

@app.route('/delete_client/<id>', methods = ['GET','POST'])
def delete_client(id):
    db.delete_client(id)
    return redirect(url_for("client"))

@app.route('/delete_distributor/<id>', methods = ['GET','POST'])
def delete_distributor(id):
    db.delete_distributor(id)
    return redirect(url_for("distributor"))

@app.route('/delete_sales/<id>', methods = ['GET','POST'])
def delete_sales(id):
    db.delete_sales(id)
    return redirect(url_for("sales"))

@app.route('/delete_purchase/<id>', methods = ['GET','POST'])
def delete_purchase(id):
    db.delete_purchase(id)
    return redirect(url_for("purchase"))

@app.route('/delete_product/<id>', methods = ['GET','POST'])
def delete_product(id):
    db.delete_product(id)
    return redirect(url_for("products"))

# @app.route('/edit_sales/<id>', methods = ['GET','POST'])
# def edit_sales(id):
#     db.edit_sales(id)
#     return redirect(url_for("sales"))
